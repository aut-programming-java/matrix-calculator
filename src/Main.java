import java.util.Scanner;

/**
 *This Program can read two matrix and
 * with inputting a phrase, program produce a matrix that calculated
 * two matrix with operator +,- or *
 *
 * This program has a professional error handler :)
 *
 * @author Alireza Mazochi
 * @version 1.0.0
 * @since 1397/01/02(Happy New year!!)
 */
public class Main {
    /**
     * This method is starting point of application
     * @param args input values in command line
     */
    public static void main(String args[]){
        Matrix matrixX=new Matrix();
        Matrix matrixY=new Matrix();
        Scanner scanner=new Scanner(System.in);
        System.out.println("Define the first matrix (X):");
        while(matrixX.input()==false){
            matrixX=new Matrix();
            System.out.println("ERROR::input matrix X not valid");
        }
        System.out.println("Define the first matrix (Y):");
        while(matrixY.input()==false){
            matrixY=new Matrix();
            System.out.println("ERROR::input matrix Y not valid");
        }
        System.out.println("Enter your polynomial expression:");
        String order;
        order=scanner.nextLine();
        char orderArray[]=order.toCharArray();
        int coefficient=0;
        int signCoefficient=+1;
        int level=0;
        char operator=' ';
        int stateOfXY=0;
        /*
        state Of X & Y
            1 --> X
            2 --> Y
            3 --> XY
            4 --> YX
         */


        //It reads  X,Y, coefficients and operator
        for(int i=0 ; i<orderArray.length ; i++){
            switch (level){
                case 0:
                    if(orderArray[i]=='X' || orderArray[i]=='Y'){
                        level++;
                        if(orderArray[i]=='X'){
                            matrixX.coefficient(coefficient*signCoefficient);
                            stateOfXY=1;
                        }
                        else{
                            matrixY.coefficient(coefficient*signCoefficient);
                            stateOfXY=2;
                        }
                        coefficient=0;
                    }
                    else{
                        if(orderArray[i]=='-'){
                            signCoefficient*=-1;
                        }
                        if('0'<=orderArray[i] && orderArray[i]<='9'){
                            coefficient*=10;
                            coefficient+=orderArray[i]-'0';
                        }
                    }
                    break;
                case 1:
                    if(orderArray[i]!=' '){
                        level++;
                        operator=orderArray[i];
                    }
                    break;
                case 2:
                    if(orderArray[i]!=' '){
                        if(orderArray[i]=='X' || orderArray[i]=='Y'){
                            level++;
                            if(orderArray[i]=='X'){
                                matrixX.coefficient(coefficient*signCoefficient);
                                stateOfXY=4;
                            }
                            else{
                                matrixY.coefficient(coefficient*signCoefficient);
                                stateOfXY=3;
                            }
                        }
                        else{
                            if(orderArray[i]=='-'){
                                signCoefficient*=-1;
                            }
                            if('0'<=orderArray[i] && orderArray[i]<='9'){
                                coefficient*=10;
                                coefficient+=orderArray[i]-'0';
                            }
                        }
                    }
                    break;

            }


        }
        Matrix result=null;
        switch (stateOfXY){
            case 1:
                result=matrixX;
                break;
            case 2:
                result=matrixY;
                break;
            case 3:
                result=matrixX.operate(matrixY,operator);
                break;
            case 4:
                result=matrixX.operate(matrixY,operator);
                break;
        }

        if(result==null){
            System.out.println("ERROR::Size of matrix or operator not valid! ");
        }
        else{
            System.out.println("Result:");
            result.print();
        }
    }
}
