import sun.nio.cs.ext.MacArabic;
import javax.swing.*;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * This class has a array for store dates of a matrix with any size
 * It has many methods for calculate with other matrix
 *
 * @author Alireza Mazochi
 * @version 1.0.0
 * @since 1397/01/02(Happy New year!!)
 */
public class Matrix {
    private int numOfColumn;
    private int numOfRow;
    private ArrayList<ArrayList<Integer>> date;

    /**
     * Constructor for class Matrix
     *
     */
    public Matrix(){
        date=new ArrayList<>();
    }

    /**
     * This method reads many strings and with special jobs
     * realize elements of matrix
     *
     * @return if inputting dates is successful or not
     */
    public boolean input(){
        while(true){
            String inp;
            Scanner scanner=new Scanner(System.in);
            ArrayList<Integer> newRow=new ArrayList<>();
            inp=scanner.nextLine();
            boolean distanceComma=false;
            if(inp.isEmpty()){
                break;
            }
            else{
                int newElement=0;
                char[] inpArray=inp.toCharArray();
                for(int i=0 ; i<inpArray.length ; i++){
                    if(inpArray[i]==','){
                        if(distanceComma==true){
                            newRow.add(newElement);
                            newElement=0;
                            distanceComma=false;
                        }
                        else{
                            return false;
                        }
                    }
                    else{
                        if('0'<=inpArray[i] && inpArray[i]<='9'){
                            distanceComma=true;
                            newElement*=10;
                            newElement+=inpArray[i]-'0';
                        }
                        else{
                            return false;
                        }
                    }
                }
                newRow.add(newElement);
                newElement=0;
                date.add(newRow);
            }
        }
        return check();
    }

    /**
     *
     * @param c a new coefficient for matrix
     */
    public void coefficient(int c){
        for(int i=0 ; i<numOfRow ; i++){
            for(int j=0 ; j<numOfColumn ; j++){
                Integer integer= date.get(i).get(j);
                integer *= c;
                date.get(i).set(j,integer);
            }
        }
    }

    /**
     * Show dates of matrix with comma
     */
    public void print(){
        for(int i=0 ; i<date.size() ; i++){
            for(int j=0 ; j<date.get(i).size() ; j++){
                System.out.print(date.get(i).get(j));
                if(j<date.get(i).size()-1){
                    System.out.print(",");
                }
            }
            System.out.println();
        }
    }

    /**
     * This method does two jobs:
     * first check size of matrix is valuable or not
     * second calculates number of rows and columns
     *
     * @return if size of matrix is valuable or not
     */
    public boolean check(){
        if(date.size()==0 && date.get(0).size()==0){
            return false;
        }
        numOfRow=date.size();
        numOfColumn=date.get(0).size();
        for(int i=1 ; i<date.size() ; i++){
            if(date.get(i).size()!= date.get(0).size()){
                return false;
            }
        }
        return true;
    }

    /**
     * plus (or minus) between two matrix
     *
     * @param other second matrix that plus or minus by current matrix
     * @return result of two matrix and if this job is not available return null
     */
    public Matrix plus(Matrix other){
        if(numOfColumn!=other.numOfRow && numOfRow!=other.numOfRow){
            return null;
        }
        Matrix output=new Matrix();
        for(int i=0 ; i<numOfRow ; i++){
            ArrayList<Integer> newRaw=new ArrayList<Integer>();
            for(int j=0 ; j<numOfColumn ;j++){
                newRaw.add(date.get(i).get(j)+other.date.get(i).get(j));
            }
            output.date.add(newRaw);
        }
        check();
        return output;
    }

    /**
     * multiplication between two matrix
     *
     * @param other second matrix that multiplication by current matrix
     * @return result of two matrix and if this job is not available return null
     */
    public Matrix multiplication(Matrix other){
        if(numOfColumn!=other.numOfRow){
            return null;
        }
        Matrix output=new Matrix();
        for(int i=0 ; i<numOfRow ; i++){
            ArrayList<Integer> newRaw=new ArrayList<Integer>();
            for(int j=0 ; j<other.numOfColumn ; j++){
                int sum=0;
                for(int k=0 ; k<numOfColumn ; k++){
                    sum+=date.get(i).get(k) * other.date.get(k).get(j);
                }
                newRaw.add(sum);
            }
            output.date.add(newRaw);
        }
        output.check();
        return output;
    }

    /**
     * a operation between two matrix
     *
     * @param other second matrix that multiplication by current matrix
     * @param operator an unknown operator between two matrix
     * @return result of two matrix and if this job is not available return null
     */
    public Matrix operate(Matrix other,char operator){
        Matrix result=null;
        switch (operator) {
            case '+':
                result = plus(other);
                break;
            case '-':
                other.coefficient(-1);
                result = plus(other);
                break;
            case '*':
                result = multiplication(other);
                break;
            default:
                result=null;
                break;
        }
        return result;
    }
}
